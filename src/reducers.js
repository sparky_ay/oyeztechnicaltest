import { combineReducers } from "redux";
import albums from "./reducers/albums";
import photos from "./reducers/photos";
import wishlist from "./reducers/wishList";
import currentPhoto from "./reducers/currentPhoto";
//comine the diffrent reducers to pass it to pass it to the store
const reducers = combineReducers({
  albums,
  photos,
  currentPhoto,
  wishlist
});

export default reducers;
