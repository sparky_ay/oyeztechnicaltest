import React from "react";
import { connect } from "react-redux";

import AlbumsList from "../components/AlbumsList";

import { getAlbums } from "../actions";

class AlbumsListContainer extends React.Component {
  componentDidMount() {
    this.props.getAlbums();
  }

  render() {
    return <AlbumsList albums={this.props.albums} />
  }
}

const mapStateToProps = state => {
  return {
    albums: state.albums
  };
};

const mapActionsToProps = dispatch => {
  return {
    getAlbums: () => {
      dispatch(getAlbums());
    }
  };
};

export default connect(
  mapStateToProps,
  mapActionsToProps
)(AlbumsListContainer);
