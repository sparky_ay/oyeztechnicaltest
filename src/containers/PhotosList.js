import React from "react";
import { connect } from "react-redux";

import PhotosList from "../components/PhotosList";

import { getPhotosForAlbum } from "../actions";

class AlbumsListContainer extends React.Component {
  componentDidMount() {
    this.props.getPhotosForAlbum(this.props.match.params.id);
  }
  // get the list of photos 
  render() {
    return <PhotosList photos={this.props.photos} />
  }
}

const mapStateToProps = state => {
  return {
    photos: state.photos
  };
};

const mapActionsToProps = dispatch => {
  return {
    getPhotosForAlbum: id => {
      dispatch(getPhotosForAlbum(id));
    }
  };
};

export default connect(
  mapStateToProps,
  mapActionsToProps
)(AlbumsListContainer);
