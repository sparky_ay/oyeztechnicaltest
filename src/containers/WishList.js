import { connect } from "react-redux";

import PhotosList from "../components/PhotosList";

const mapStateToProps = state => {
  return {
    photos: state.wishlist
  }
}

export default connect(mapStateToProps)(PhotosList);
