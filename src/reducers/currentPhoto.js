//current photo reducer
export default (state = {}, action) => {
  switch (action.type) {
    case "SET_CURRENT_PHOTO":
      return action.payload;
    default:
      return state;
  }
};
