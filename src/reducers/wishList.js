//wishlist reducer
export default (state = [], action) => {
  switch (action.type) {
    //
    case "SET_WISHLIST":
      return action.payload;
    //add photo to wishlist array
    case "ADD_WHISH":
      return state.includes(action.payload)
        ? state
        : [...state, ...[action.payload]];
    default:
      return state;
  }
}
