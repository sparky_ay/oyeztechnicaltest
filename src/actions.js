import axios from "axios";

const url = path => `https://jsonplaceholder.typicode.com${path}`;
//http get request to the albums api
export const getAlbums = () => {
  return async dispatch => {
    let albums = await axios.get(url("/albums"));
    albums = albums.data;
    dispatch({
      type: "SET_ALBUMS",
      payload: albums
    });
  };
};
//http get request to the list of photos for the specified albumId
export const getPhotosForAlbum = albumId => {
  return async dispatch => {
    let photos = await axios.get(url(`/albums/${albumId}/photos`));
    photos = photos.data;
    dispatch({
      type: "SET_PHOTOS",
      payload: photos
    });
  };
};
//http get request to the specified photoID
export const getCurrentPhoto = photoId => {
  return async dispatch => {
    let photo = await axios.get(url(`/photos/${photoId}`));
    photo = photo.data;
    dispatch({
      type: "SET_CURRENT_PHOTO",
      payload: photo
    });
  };
};
//Add photo to wishlist 
export const addToWishlist = photo => {
  return dispatch => {
    dispatch({
      type: "ADD_WHISH",
      payload: photo
    });
  };
};
