import { createStore, applyMiddleware } from "redux";
import reducers from "./reducers";
import thunk from "redux-thunk";
//create store and apply the thunk middelware to handle async function
const store = createStore(reducers, applyMiddleware(thunk));

export default store;
