import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import { Provider } from "react-redux";
import store from "./store";

import AlbumsList from "./containers/AlbumsList";
import PhotoDetails from "./containers/PhotoDetails";
import PhotosList from "./containers/PhotosList";
import WishList from "./containers/WishList";

function App() {
  return (
    <Router>
      <div>
        <div style={{backgroundColor: '#333',padding:'10px 10px',marginTop:'0px', width:'100%',textAlign:'center'}}>

            <Link to="/" style={{fontWeight: 'bold',color: 'white',padding:'10px', textDecoration: 'none' }}>Home</Link>

            <Link to="/wishlist" style={{ fontWeight: 'bold',color: 'white', textDecoration: 'none' }}>Whishlist</Link>

        </div>
        <div style={{paddingTop:'2  %'}}>
          <Route exact path="/" component={AlbumsList} />
          <Route exact path="/albums" component={AlbumsList} />
          <Route exact path="/albums/:id" component={PhotosList} />
          <Route path="/photos/:id" component={PhotoDetails} />
          <Route path="/wishlist" component={WishList} />
        </div>
      </div>
    </Router>
  );
}
const rootElement = document.getElementById("root");
ReactDOM.render(
  <Provider store={store}>
    <App />
  </Provider>,
  rootElement
);
