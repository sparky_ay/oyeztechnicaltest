import React from "react";
import { Link } from "react-router-dom";

export default ({ photos }) => (
  <div>
    {photos.map(photo => {
      return (
        <Link to={`/photos/${photo.id}`}>
          <img src={photo.thumbnailUrl} />
        </Link>
      );
    })}
  </div>
);
