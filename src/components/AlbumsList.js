import React from "react";
import { Link } from "react-router-dom";
export default ({ albums }) => (
  <div>
    <ul>
      {albums.map(album => (
        <li>
          <Link to={`/albums/${album.id}`}>{album.title}</Link>
        </li>
      ))}
    </ul>
  </div>
);
