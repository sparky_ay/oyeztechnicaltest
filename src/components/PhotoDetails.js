import React from "react";
export default ({ photo, addToWishlist }) => (
  <div>
    <div  style={{float:'left',width: '50%'}}>
      <img src={photo.url} style={{height:'auto'}}/>
    </div>
    <div style={{float:'right',width: '50%'}}>
      <h2  >Title: {photo.title}</h2>
      <h5 style={{}} >Link: {photo.url}</h5>
      <button onClick={() => addToWishlist(photo)}>Add to whishlist</button>
    </div>
  </div>
);
